﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentDemo1
{
    interface ShapeInterface
    {
        void SetPosition(int x, int y);
        void Draw(Graphics g, Pen myPen);
        string ToString();
    }
}
