﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using Demo.Library;
using System.Collections;

namespace AssignmentDemo1
{
    public partial class Form1 : Form
    {
        static int curX = 0, curY = 0, param1 = 0, param2 = 0, param3 = 0, param4 = 0;
        static Pen myPen = new Pen(Color.Black, 2);
        static Pen myPos = new Pen(Color.Red, 1); //draw moveto
        static Pen erasePos = new Pen(Color.LightGray, 2); //erase moveto
        static CommandParser myParser = new CommandParser();

        static Point origin1 = new Point(0, 0); //reset position
        static Point origin2 = new Point(0, 0);
        ArrayList shapes = new ArrayList();

        public Form1()
        {
            InitializeComponent();
            Canvas.BackColor = Color.LightGray;
        }

        private void FontDialog1_Apply(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Canvas_Paint(object sender, PaintEventArgs e)
        {

        }

        private void CommandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                paintingCanvas(CommandLine.Text);
                CommandLine.Clear();
            } //if
        }

        private void paintingCanvas(string command)
        {
            Graphics g = null;
            g = Canvas.CreateGraphics();

            //command = CommandLine.Text; //used to execute command earlier
            command = command.ToLower();
            string[] commandArray = command.Split(' ');

            switch (commandArray[0])
            {
                case "drawto":
                    try
                    {
                        //parameter checking
                        myParser.CheckParams(commandArray, ref param1, ref param2);

                        //Prompt command to record
                        PrompLine.SelectionStart = PrompLine.Text.Length;
                        PrompLine.SelectedText = command + Environment.NewLine;
                        PrompLine.ScrollToCaret();

                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(myPen, curX, curY, param1, param2);
                        curX = param1;
                        curY = param2;
                        g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;
                case "moveto":
                    try
                    {
                        //parameter checking
                        myParser.CheckParams(commandArray, ref param1, ref param2);

                        //Prompt command to record
                        PrompLine.SelectionStart = PrompLine.Text.Length;
                        PrompLine.SelectedText = command + Environment.NewLine;
                        PrompLine.ScrollToCaret();

                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        g.DrawLine(myPen, param1, param2, param1, param2);
                        curX = param1;
                        curY = param2;
                        MessageBox.Show("New position:" + curX + ", " + curY);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;
                case "show":
                    g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                    MessageBox.Show("Current position:" + curX + ", " + curY);
                    break;
                case "rect":
                    try
                    {
                        //parameter checking
                        myParser.CheckParams(commandArray, ref param1, ref param2);

                        //Prompt command to record
                        PrompLine.SelectionStart = PrompLine.Text.Length;
                        PrompLine.SelectedText = command + Environment.NewLine;
                        PrompLine.ScrollToCaret();

                        /*
                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        g.DrawRectangle(myPen, curX, curY, param1, param2);
                        */
                        ShapeRect Rect1 = new ShapeRect();
                        Rect1.SetPosition(curX - 5, curY - 5);
                        Rect1.SetSize(param1, param2);
                        Rect1.ClearMark(g, erasePos, curX, curY);
                        Rect1.Draw(g, myPen);
                        curX = curX + param1;
                        curY = curY + param2;
                        Rect1 = null; //garbage collection
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;
                case "clear":
                    try
                    {
                        Canvas.Refresh(); //clean canvas
                        PrompLine.Clear(); //clean textbox
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;
                case "reset":
                    try
                    {
                        g.DrawLine(myPen, origin1, origin2);
                        curX = 0;
                        curY = 0;
                        MessageBox.Show("Pen position is reset");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;
                case "fr":
                    try
                    {
                        Canvas.Refresh(); //clean canvas
                        PrompLine.Clear(); //clean textbox
                        g.DrawLine(myPen, origin1, origin2);
                        curX = 0;
                        curY = 0;
                        MessageBox.Show("All reset complete");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;
                case "circle":
                    try
                    {
                        //parameter checking
                        myParser.CheckParams(commandArray, ref param1, ref param2);

                        //Prompt command to record
                        PrompLine.SelectionStart = PrompLine.Text.Length;
                        PrompLine.SelectedText = command + Environment.NewLine;
                        PrompLine.ScrollToCaret();

                        /*
                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        g.DrawEllipse(myPen, curX, curY, param1, param2);
                        */
                        ShapeCircle Circle1 = new ShapeCircle();
                        Circle1.SetPosition(curX - 5, curY - 5);
                        Circle1.SetSize(param1, param2);
                        Circle1.ClearMark(g, erasePos, curX, curY);
                        Circle1.Draw(g, myPen);
                        curX = Casepack.myCircleX(curX, param1);
                        curY = Casepack.myCircleY(curY, param2);
                        Circle1 = null; //garbage collection
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;
                case "triangle":
                    try
                    {
                        //parameter checking
                        myParser.CheckTriParams(commandArray, ref param1, ref param2, ref param3, ref param4);

                        //Prompt command to record
                        PrompLine.SelectionStart = PrompLine.Text.Length;
                        PrompLine.SelectedText = command + Environment.NewLine;
                        PrompLine.ScrollToCaret();

                        /*
                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        Point p1 = new Point(curX, curY);
                        Point p2 = new Point(param1, param2);
                        Point p3 = new Point(param3, param4);

                        g.DrawLine(myPen, p1, p2);
                        g.DrawLine(myPen, p2, p3);
                        g.DrawLine(myPen, p3, p1);
                        */
                        ShapeTri Tri1 = new ShapeTri();
                        Tri1.SetPosition(curX - 5, curY - 5);
                        Tri1.SetSize(param1, param2, param3, param4);
                        Tri1.ClearMark(g, erasePos, curX, curY);
                        Tri1.Draw(g, myPen);
                        Tri1 = null; //garbage collection
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;
                case "save":
                    try
                    {
                        SaveFileDialog saveTxt = new SaveFileDialog();
                        saveTxt.Title = "Save to...";
                        saveTxt.InitialDirectory = ".\\";
                        saveTxt.Filter = "Txt files (*.*)|*.txt";

                        if (saveTxt.ShowDialog() == DialogResult.OK)
                        {
                            TextWriter Twriter = new StreamWriter(saveTxt.OpenFile());
                            Twriter.Write(PrompLine.Text);
                            Twriter.Close();
                            PrompLine.SelectionStart = PrompLine.Text.Length;
                            PrompLine.SelectedText = "Save completed" + Environment.NewLine;
                            PrompLine.ScrollToCaret();
                        } else
                        {
                            MessageBox.Show("Save Aborted");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;
                case "load":
                    try
                    {
                        OpenFileDialog openTxt = new OpenFileDialog();
                        openTxt.Title = "Open...";
                        openTxt.InitialDirectory = ".\\";
                        openTxt.Filter = "Txt files (*.*)|*.txt";

                        if (openTxt.ShowDialog() == DialogResult.OK)
                        {
                            string txtName = openTxt.FileName;
                            List<string> prog = File.ReadAllLines(txtName).ToList();
                            foreach (string line in prog)
                            {
                                string commandLoad = line;
                                paintingCanvas(line);
                            }
                            MessageBox.Show("Loaded command");
                        } else
                        {
                            MessageBox.Show("Load Aborted");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;
                case "color":
                    try
                    {
                        //parameter checking
                        myParser.CheckColorParams(commandArray, ref param1, ref param2, ref param3);
                        myPen.Color = Color.FromArgb(param1, param2, param3);

                        //Message box to show new color
                        MessageBox.Show("Pen color changed to: R" + param1 + " G" + param2 + " B" + param3);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;
                case "exit":
                    if (System.Windows.Forms.Application.MessageLoop)
                    {
                        // WinForms app
                        System.Windows.Forms.Application.Exit();
                    }
                    break;
                case "mcircle":
                    try
                    {
                        //parameter checking
                        myParser.CheckMParams(commandArray, ref param1, ref param2, ref param3);

                        //Prompt command to record
                        PrompLine.SelectionStart = PrompLine.Text.Length;
                        PrompLine.SelectedText = command + Environment.NewLine;
                        PrompLine.ScrollToCaret();

                        /*
                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        g.DrawEllipse(myPen, curX, curY, param1, param2);
                        */
                        ShapeCircle Circle1 = new ShapeCircle();
                        for (int i = 0; i < param3; i++)
                        {
                            Circle1.SetPosition(curX - 5, curY - 5);
                            Circle1.SetSize(param1, param2);
                            Circle1.ClearMark(g, erasePos, curX, curY);
                            Circle1.Draw(g, myPen);
                            curX = Casepack.myCircleX(curX, param1);
                            curY = Casepack.myCircleY(curY, param2);
                        }
                        Circle1 = null; //garbage collection
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;
                default:
                    MessageBox.Show("Error input");
                    break;
            } //Switch

        }

        private void CommandLine_TextChanged(object sender, EventArgs e)
        {

        }
        private void PrompLine_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
