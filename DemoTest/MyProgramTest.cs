﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Demo.Library;

namespace DemoTest
{
    [TestClass]
    public class MyProgramTest
    {
        [TestMethod]
        public void Test_CircleX()
        {
            //Arrange
            int expect = 80;
            int cur1 = 60;
            int param1 = 20;

            //Act
            int actual = Casepack.myCircleX(cur1, param1);

            //Assert
            Assert.AreEqual(expect, actual);
        } //Test CircleX

        [TestMethod]
        public void Test_CircleY()
        {
            //Arrangr
            int expect = 80;
            int cur2 = 60;
            int param2 = 20;

            //Act
            int actual = Casepack.myCircleY(cur2, param2);

            //Assert
            Assert.AreEqual(expect, actual);
        }

    }
}
