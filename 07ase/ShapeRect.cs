﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace AssignmentDemo1
{
    class ShapeRect
    {
        private int x, y, param1, param2;

        public void SetPosition(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public void SetSize(int param1, int param2)
        {
            this.param1 = param1;
            this.param2 = param2;
        }
        public void ClearMark(Graphics g, Pen erasePos, int x, int y)
        {
            g.DrawEllipse(erasePos, x - 5, y - 5, 10, 10);
        }

        public void Draw(Graphics g, Pen myPen)
        {
            g.DrawRectangle(myPen, x, y, param1, param2);
        }
    }
}
