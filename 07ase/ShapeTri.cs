﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace AssignmentDemo1
{
    class ShapeTri
    {
        private int x, y, param1, param2, param3, param4;

        public void SetPosition(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public void SetSize(int param1, int param2, int param3, int param4)
        {
            this.param1 = param1;
            this.param2 = param2;
            this.param3 = param3;
            this.param4 = param4;
        }
        public void ClearMark(Graphics g, Pen erasePos, int x, int y)
        {
            g.DrawEllipse(erasePos, x - 5, y - 5, 10, 10);
        }

        public void Draw(Graphics g, Pen myPen)
        {
            Point p1 = new Point(x, y);
            Point p2 = new Point(param1, param2);
            Point p3 = new Point(param3, param4);
            g.DrawLine(myPen, p1, p2);
            g.DrawLine(myPen, p2, p3);
            g.DrawLine(myPen, p3, p1);
        }
    }
}
