﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentDemo1
{
    class CommandParser
    {
        /// <summary>
        /// The command parser and check system of normal commands
        /// </summary>
        /// <param name="command">Command log</param>
        /// <param name="arg1">point X</param>
        /// <param name="arg2">point Y</param>
        public void CheckParams(string[] command, ref int arg1, ref int arg2)
        {
            if (command.Length != 3)
            {
                throw new Exception("Command parameters error");
            }
            else if (!int.TryParse(command[1], out arg1))
            {
                throw new Exception("Command parameter 1 error");
            }
            else if (!int.TryParse(command[2], out arg2))
            {
                throw new Exception("Command parameter 2 error");
            } //if end
            arg1 = Int32.Parse(command[1]);
            arg2 = Int32.Parse(command[2]);
        } // method end

        /// <summary>
        /// Parser for color change
        /// </summary>
        /// <param name="command">color change command</param>
        /// <param name="arg1">Red</param>
        /// <param name="arg2">Green</param>
        /// <param name="arg3">Blue</param>
        public void CheckColorParams(string[] command, ref int arg1, ref int arg2, ref int arg3)
        {
            if (command.Length != 4)
            {
                throw new Exception("Command parameters error");
            }
            else if (!int.TryParse(command[1], out arg1))
            {
                throw new Exception("Command parameter 1 error");
            }
            else if (!int.TryParse(command[2], out arg2))
            {
                throw new Exception("Command parameter 2 error");
            }
            else if (!int.TryParse(command[3], out arg3))
            {
                throw new Exception("Command parameter 3 error");
            } //if end
            arg1 = Int32.Parse(command[1]);
            arg2 = Int32.Parse(command[2]);
            arg3 = Int32.Parse(command[3]);
        } // method end

        /// <summary>
        /// Parser for triangle command
        /// </summary>
        /// <param name="command">Triangle</param>
        /// <param name="arg1">from start point</param>
        /// <param name="arg2">from arg1</param>
        /// <param name="arg3">from arg2</param>
        /// <param name="arg4">from arg3</param>
        public void CheckTriParams(string[] command, ref int arg1, ref int arg2, ref int arg3, ref int arg4)
        {
            if (command.Length != 5)
            {
                throw new Exception("Command parameters error");
            }
            else if (!int.TryParse(command[1], out arg1))
            {
                throw new Exception("Command parameter 1 error");
            }
            else if (!int.TryParse(command[2], out arg2))
            {
                throw new Exception("Command parameter 2 error");
            }
            else if (!int.TryParse(command[3], out arg3))
            {
                throw new Exception("Command parameter 3 error");
            }
            else if (!int.TryParse(command[4], out arg4))
            {
                throw new Exception("Command parameter 4 error");
            } //if end
            arg1 = Int32.Parse(command[1]);
            arg2 = Int32.Parse(command[2]);
            arg3 = Int32.Parse(command[3]);
            arg4 = Int32.Parse(command[4]);
        } // method end

        public void CheckMParams(string[] command, ref int arg1, ref int arg2, ref int arg3)
        {
            if (command.Length != 4)
            {
                throw new Exception("Command parameters error");
            }
            else if (!int.TryParse(command[1], out arg1))
            {
                throw new Exception("Command parameter 1 error");
            }
            else if (!int.TryParse(command[2], out arg2))
            {
                throw new Exception("Command parameter 2 error");
            }
            else if (!int.TryParse(command[3], out arg3))
            {
                throw new Exception("Command parameter 3 error");
            } //if end
            arg1 = Int32.Parse(command[1]);
            arg2 = Int32.Parse(command[2]);
            arg3 = Int32.Parse(command[3]);
        } // method end
    } // class end


} // class end
