﻿namespace AssignmentDemo1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Canvas = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.PrompLine = new System.Windows.Forms.TextBox();
            this.CommandLine = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Canvas
            // 
            this.Canvas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Canvas.Location = new System.Drawing.Point(0, 0);
            this.Canvas.Name = "Canvas";
            this.Canvas.Size = new System.Drawing.Size(800, 415);
            this.Canvas.TabIndex = 0;
            this.Canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.Canvas_Paint);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.PrompLine);
            this.panel2.Controls.Add(this.CommandLine);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 288);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(800, 127);
            this.panel2.TabIndex = 0;
            // 
            // PrompLine
            // 
            this.PrompLine.BackColor = System.Drawing.Color.Black;
            this.PrompLine.ForeColor = System.Drawing.Color.White;
            this.PrompLine.Location = new System.Drawing.Point(409, 3);
            this.PrompLine.Multiline = true;
            this.PrompLine.Name = "PrompLine";
            this.PrompLine.ReadOnly = true;
            this.PrompLine.Size = new System.Drawing.Size(388, 122);
            this.PrompLine.TabIndex = 1;
            this.PrompLine.TextChanged += new System.EventHandler(this.PrompLine_TextChanged);
            // 
            // CommandLine
            // 
            this.CommandLine.Location = new System.Drawing.Point(3, 27);
            this.CommandLine.Name = "CommandLine";
            this.CommandLine.Size = new System.Drawing.Size(403, 22);
            this.CommandLine.TabIndex = 0;
            this.CommandLine.TextChanged += new System.EventHandler(this.CommandLine_TextChanged);
            this.CommandLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CommandLine_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "Enter your command:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 415);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.Canvas);
            this.Name = "Form1";
            this.Text = "Canvas";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Canvas;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox CommandLine;
        private System.Windows.Forms.TextBox PrompLine;
        private System.Windows.Forms.Label label1;
    }
}

